/**
 * First test class is a test class, this truly is TDD
 * @date 8/19/2014
 * @author James Loghry
 */
@isTest
private class TrulyTDD{

	/**
	 * Placeholder test class for my first method.
	 */
	static testmethod void testMyFirstMethod(){
		System.assert(false,'Not implemented yet.');
	}

	/**
	 * Placeholder test class for my first method.
	 */
	static testmethod void testMySecondMethod(){
		System.assert(false,'Not implemented yet.');
	}

	//
}
